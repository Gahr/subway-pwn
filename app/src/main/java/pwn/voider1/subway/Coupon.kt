package pwn.voider1.subway

data class Coupon(val name: String, val disclaimer: String, val image: Int)
