package pwn.voider1.subway

enum class Action {
    GETCOUPONS,
    SETCOUPON,
    RESETCOUPON,
    CLEARCOUPON,
    UNKNOWN;
}

fun String?.toAction(): Action {
    return when (this) {
        "GETCOUPONS" -> Action.GETCOUPONS
        "SETCOUPON" -> Action.SETCOUPON
        "RESETCOUPON" -> Action.RESETCOUPON
        "CLEARCOUPON" -> Action.CLEARCOUPON
        else -> Action.UNKNOWN
    }
}
