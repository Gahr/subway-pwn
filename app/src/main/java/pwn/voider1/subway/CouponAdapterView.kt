package pwn.voider1.subway

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.github.lzyzsd.randomcolor.RandomColor

class CouponAdapterView(private val context: Context, private val coupons: Array<Sub>) : BaseAdapter() {

    override fun getItem(p0: Int): Any {
        return coupons[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return coupons.size
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var rowView = p1

        if (rowView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            rowView = inflater.inflate(R.layout.coupon_layout, null)

            val viewHolder = ViewHolder()
            viewHolder.couponText = rowView!!.findViewById(R.id.couponText)
            viewHolder.couponCard = rowView.findViewById(R.id.couponCard)

            rowView.tag = viewHolder
        }
        val type = Typeface.createFromAsset(context.assets, "Subway-Footlong.otf")

        val holder = rowView.tag as ViewHolder

        holder.couponText!!.text = coupons[p0].name
        holder.couponText!!.typeface = type

        val randomColor = RandomColor()
        val color = randomColor.randomColor()

        holder.couponCard?.setColorFilter(manipulateColor(color, 0.5f), android.graphics.PorterDuff.Mode.SRC_IN)

        return rowView
    }

    internal class ViewHolder {
        var couponText: TextView? = null
        var couponCard: ImageView? = null
    }

    private fun manipulateColor(color: Int, factor: Float): Int {
        val a = Color.alpha(color)
        val r = Math.round(Color.red(color) * factor)
        val g = Math.round(Color.green(color) * factor)
        val b = Math.round(Color.blue(color) * factor)
        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255))
    }

}
