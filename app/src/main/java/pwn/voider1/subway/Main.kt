package pwn.voider1.subway

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Resources
import android.content.res.XModuleResources
import android.content.res.XResources
import de.robv.android.xposed.*
import de.robv.android.xposed.XposedHelpers.*
import de.robv.android.xposed.callbacks.XC_InitPackageResources
import de.robv.android.xposed.callbacks.XC_LoadPackage
import java.util.*

class Main : IXposedHookLoadPackage, IXposedHookInitPackageResources, IXposedHookZygoteInit {
    private val packageName = "nl.subway.subway"
    private val subwayBasePath = "com.esites.subway.a"
    private val sharedPrefsName = "coupon"

    private val getRandomCoupon = "d"

    private var coupon30CmMenuId = 0
    private val coupon30CmMenuName = "coupon_30cm_menu"
    private val coupon30CmMenuType = "drawable"

    private var couponIndex: Int? = null

    private lateinit var lpparam: XC_LoadPackage.LoadPackageParam
    private lateinit var xres: XResources
    private lateinit var modRes: XModuleResources
    private lateinit var modulePath: String

    private var context: Context? = null
        set(value) {
            if (context == null) {
                value?.let { ctx ->
                    setupBroadcastReceiver(ctx)
                    field = ctx
                    logInfo("Retrieved context")
                }
            }
        }

    private val fakeCoupons = arrayOf(
            Coupon(
                    "Chicken Teriyaki of Subway Melt",
                    "Deze coupon is 24 uur geldig (zie countdown) in deelnemende Subway® restaurants in Nederland. Geef vóór het plaatsen van je bestelling aan dat je gebruik wilt maken van de coupon. Geldig voor de 15 cm Chicken Teriyaki óf Subway Melt (combineren is ook mogelijk). Bij combineren is de goedkoopste Sub gratis. Niet geldig voor dubbel vlees, extra kaas, bacon, pepperoni of avocado. Niet geldig i.c.m. andere aanbiedingen. Eén coupon per klant per bezoek. Bij deze actie worden geen spaarzegels verstrekt. SUBWAY® is a Registered Trademark of Subway IP Inc. ©2018 Subway IP Inc.",
                    R.drawable.coupon_yaki_melt
            )
    )

    fun logInfo(msg: String) {
        XposedBridge.log("[Subway PWN INFO] $msg")
    }

    fun logErr(msg: String) {
        XposedBridge.log("[Subway PWN ERROR] $msg")
    }

    override fun initZygote(startupParam: IXposedHookZygoteInit.StartupParam?) {
        startupParam?.let { s ->
            modulePath = s.modulePath.toString()
            logInfo("Retrieved module path: $modulePath")
        } ?: logErr("startupParam is unavailable, this cannot happen")
    }

    override fun handleInitPackageResources(resparam: XC_InitPackageResources.InitPackageResourcesParam?) {
        if (resparam?.packageName != packageName) return

        logInfo("Found $packageName's resources")
        xres = resparam.res
        modRes = XModuleResources.createInstance(modulePath, xres)
        coupon30CmMenuId = xres.getIdentifier(coupon30CmMenuName, coupon30CmMenuType, packageName)
        logInfo("Retrieved unused resource's identifier $coupon30CmMenuId of resource $coupon30CmMenuName")
    }

    override fun handleLoadPackage(lpparam: XC_LoadPackage.LoadPackageParam?) {
        if (lpparam?.packageName != packageName) return

        logInfo("Found $packageName")
        this.lpparam = lpparam

        findAndHookMethod("android.app.Application", lpparam.classLoader, "onCreate", object : XC_MethodHook() {
            override fun beforeHookedMethod(param: MethodHookParam?) {
                param?.run {
                    context = (thisObject as Application).applicationContext
                }
            }
        })

        // Hooking the method which randomly selects the coupon
        findAndHookMethod("$subwayBasePath.d", lpparam.classLoader, getRandomCoupon, object : XC_MethodHook() {
            override fun afterHookedMethod(param: MethodHookParam?) {
                logInfo("$subwayBasePath.d.$getRandomCoupon has been invoked.")

                couponIndex?.let { i ->
                    val self = param?.thisObject
                            ?: logErr("We're hooking a non-static method, param cannot be null")
                    val coupons = getObjectField(self, "a") as Array<*>
                    val coupon = coupons[i]
                    param?.result = coupon ?: logErr("This method is returning something, this can never happen!")
                    logInfo("Return value set to coupon index: $i")
                } ?: logInfo("No coupon set")
            }
        })

        // Method for reading the coupon resources
        findAndHookMethod("$subwayBasePath.c", lpparam.classLoader, "a", Resources::class.java, object : XC_MethodHook() {
            override fun afterHookedMethod(param: MethodHookParam?) {
                logInfo("$subwayBasePath.c has been invoked")

                val couponConstructor = findConstructorExact("$subwayBasePath.b", lpparam.classLoader, Int::class.java, String::class.java, String::class.java, Int::class.java)
                val couponArray = param!!.result as Array<*> // This call can never fail
                val newCouponsArray = Arrays.copyOf(couponArray, couponArray.size + fakeCoupons.size)

                for (index in couponArray.size until newCouponsArray.size) {
                    val fakeCoupon = fakeCoupons[index - couponArray.size]
                    val coupon = couponConstructor.newInstance(index + 1, fakeCoupon.name, fakeCoupon.disclaimer, coupon30CmMenuId)
                    logInfo("Created new coupon: ${fakeCoupon.name}")
                    logInfo("Before adding it: ${Arrays.toString(newCouponsArray)}")
                    newCouponsArray[index] = coupon
                    logInfo("After adding it: ${Arrays.toString(newCouponsArray)}")
                }

                param.result = newCouponsArray
                logInfo("Set new coupon array")
            }
        })

        findAndHookMethod("android.text.TextUtils ", lpparam.classLoader, "isEmpty", CharSequence::class.java, object : XC_MethodHook() {
            override fun afterHookedMethod(param: MethodHookParam?) {
                param?.args!![0]?.run {
                    if (toString() == "https://www.subwaycampaigns.nl/campaign") {
                        val result = param.result as Boolean
                        param.result = !result
                    }
                }
            }
        })
    }


    private fun setupBroadcastReceiver(context: Context) {
        val filter = IntentFilter()
        val couponReadClass = findClass("$subwayBasePath.c", lpparam.classLoader)

        filter.addAction(Action.GETCOUPONS.toString())
        filter.addAction(Action.SETCOUPON.toString())
        filter.addAction(Action.RESETCOUPON.toString())
        filter.addAction(Action.CLEARCOUPON.toString())

        val receiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent?) {
                val action = intent!!.action.toAction()  // The IntentFilter assures there's always an action
                logInfo("Received intent: $action")

                when (action) {
                    Action.GETCOUPONS -> {
                        val coupons = callStaticMethod(couponReadClass, "a", xres) as Array<*>
                        logInfo("Received COUPONS = ${Arrays.toString(coupons)}")

                        val subs = coupons.map { c ->
                            val index = getIntField(c, "a")
                            val name = getObjectField(c, "b") as String
                            "$index;$name"
                        }.toString().replace(" ", "")

                        val subsIntent = Intent()
                        subsIntent.action = "SUBS"
                        subsIntent.putExtra("subs", subs)
                        context.sendBroadcast(subsIntent)
                        logInfo("Subs send")
                    }
                    Action.SETCOUPON -> {
                        val newCouponIndex = intent.getIntExtra("coupon", -1)

                        if (newCouponIndex > -1) {
                            resetCoupon(context)
                            val coupons = callStaticMethod(couponReadClass, "a", xres) as Array<*>
                            val name = getObjectField(coupons[newCouponIndex], "b") as String

                            fakeCoupons.firstOrNull { fc -> fc.name == name }?.let {
                                xres.setReplacement(packageName, coupon30CmMenuType, coupon30CmMenuName, modRes.fwd(it.image))
                                logInfo("Set unused resource's identifier to identifier ${it.image} of resource $coupon30CmMenuName")
                            }

                            logInfo("Wrote value: $newCouponIndex, last value was: $couponIndex")
                            couponIndex = newCouponIndex
                        } else {
                            logErr("Got an invalid coupon index: $couponIndex")
                        }
                    }
                    Action.RESETCOUPON -> resetCoupon(context)
                    Action.CLEARCOUPON -> couponIndex = null
                    Action.UNKNOWN -> logErr("Got an unknown intent, there is an intent filter and this cannot happen.")
                }
            }
        }

        context.registerReceiver(receiver, filter)
        logInfo("Registered BroadcastReceiver")
    }

    private fun resetCoupon(context: Context) {
        val prefs = context.getSharedPreferences(sharedPrefsName, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
        logInfo("Coupon has been reset")
    }
}