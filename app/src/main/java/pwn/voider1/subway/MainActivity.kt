package pwn.voider1.subway

import android.content.Intent
import android.content.IntentFilter
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow

class MainActivity : AppCompatActivity() {

    private val subsAction = "SUBS"
    private val subwayPackageName = "nl.subway.subway"
    private val TAG = "MainActivity"
    private var receiver: SubReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val coupButton = findViewById<Button>(R.id.coupButton)
        val resetButton = findViewById<Button>(R.id.resetButton)
        val clearButton = findViewById<Button>(R.id.clearButton)
        val moreText = findViewById<TextView>(R.id.moreText)

        val type = Typeface.createFromAsset(assets, "Subway-Footlong.otf")
        coupButton.typeface = type
        resetButton.typeface = type
        moreText.typeface = type
        clearButton.typeface = type

        val flow = findViewById<FeatureCoverFlow>(R.id.flow)
        val filter = IntentFilter()
        receiver = SubReceiver(flow)
        filter.addAction(subsAction)
        registerReceiver(receiver, filter)

        val intent = Intent()
        intent.action = Action.GETCOUPONS.toString()
        sendBroadcast(intent)
    }

    fun getTheSubsDude(view: View) {
        val flow = findViewById<FeatureCoverFlow>(R.id.flow)
        val couponIndex = (flow.adapter.getItem(flow.scrollPosition) as Sub).index - 1

        val subIntent = Intent()
        subIntent.action = Action.SETCOUPON.toString()
        subIntent.putExtra("coupon", couponIndex)
        sendBroadcast(subIntent)

        packageManager.getLaunchIntentForPackage(subwayPackageName)?.let { startActivity(it) }
                ?: Log.e(TAG, "launchIntent cannot be null, valid arguments have been provided!")
    }

    fun reset(view: View) {
        val subIntent = Intent()
        subIntent.action = Action.RESETCOUPON.toString()
        sendBroadcast(subIntent)

        packageManager.getLaunchIntentForPackage(subwayPackageName)?.let { startActivity(it) }
                ?: Log.e(TAG, "launchIntent cannot be null, valid arguments have been provided!")
    }

    private fun clear(view: View) {
        val subIntent = Intent()
        subIntent.action = Action.CLEARCOUPON.toString()
        sendBroadcast(subIntent)

        packageManager.getLaunchIntentForPackage(subwayPackageName)?.let { startActivity(it) }
                ?: Log.e(TAG, "launchIntent cannot be null, valid arguments have been provided!")
    }
}
