package pwn.voider1.subway

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow

class SubReceiver(private val flow: FeatureCoverFlow) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action

        when (action) {
            "SUBS" -> {
                val subs = parse(intent.getStringExtra("subs"))
                flow.adapter = CouponAdapterView(context, subs)
            }
        }
    }

    private fun parse(subs: String): Array<Sub> {
        return subs
                .substring(1, subs.length - 1)
                .split(",")
                .map { pair ->
                    val vals = pair.split(";")
                    Log.d("subway", vals.toString())
                    val index = vals[0].toInt()
                    val name = vals[1]
                    Sub(index, name)
                }.toTypedArray()
    }
}
